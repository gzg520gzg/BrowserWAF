BrowserWAF（浏览器WAF）
一、简介
http://www.ShareWAF.com/BrowserWAF/
Auther：王二狗（WangErGou）
Email：6465660@qq.com

性质：开源
开发语言：JavaScript（JS）
二、功能：
运行于浏览器端的WAF、轻量化的WAF、开源WAF。用于保护网站（含H5功能页、游戏、小程序）、防多种常见网络攻击。
BrowserWAF是先驱、探索性的新型WAF，应该是互联网首个部署于浏览器端的WAF，其前身是ShareWAF（http://ShareWAF.com）的前端WAF模块。

具体防护功能：

1、防自动化攻击。如：撞库、暴力破解、批量注册、批量发贴回复、自动按键软件等；
2、指纹防护。通过大数据指纹库识别来防者，自动拦截黑名单访客；
3、防SQL注入、文件包含、目录遍历等（传统WAF功能）；
4、防CRSF攻击；
5、防Iframe框架嵌套；
6、防爬虫；
7、防XSS；
更多功能持续开发中...